package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.MedicoController;
import model.Paciente;

public class HistoricoView extends JFrame implements Observer{

	private JPanel contentPane;
	private JTable table;
	private String idPaciente;

	/**
	 * Create the frame.
	 */
	public HistoricoView(String idPaciente) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 717, 469);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		this.idPaciente = idPaciente;
		
		setTitle("Histórico:"+this.idPaciente);
		JTextPane txtpnHistricoDosPerodos = new JTextPane();
		txtpnHistricoDosPerodos.setEditable(false);
		txtpnHistricoDosPerodos.setFont(new Font("Roboto Light", Font.PLAIN, 16));
		txtpnHistricoDosPerodos.setText("Hist\u00F3rico dos per\u00EDodos de risco");
		txtpnHistricoDosPerodos.setBounds(10, 11, 500, 20);
		contentPane.add(txtpnHistricoDosPerodos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 50, 681, 369);
		contentPane.add(scrollPane);
		
		MedicoController.getInstance().addObserver(this);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Hor\u00E1rio","Batimentos","Pressão Sistólica","Pressão Diastólica","Em Repouso"
			}){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] {
				false, false,false,false,false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane.setViewportView(table);
		
		TimerTask t = new RequisitarAtualizacoes();
		final Timer timer = new Timer();
		timer.schedule(t, 0,60000);
		
		this.addWindowListener(new WindowAdapter(){
			
			@Override
			public void windowClosing(WindowEvent e){
				timer.cancel();
			}
		});
		setVisible(true);
	}
	
	private class RequisitarAtualizacoes extends TimerTask{

		@Override
		public void run() {
			MedicoController.getInstance().recuperarHistorico(idPaciente);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o != null && arg instanceof ArrayList){
			if(arg.equals(MedicoController.getInstance().getHistorico())){
				atualizarTabela();
			}
		}
	}
	
	//Atualizar Listas
	private void atualizarTabela(){
		ArrayList<String[]> historico = MedicoController.getInstance().getHistorico();
		Iterator<String[]> iterador = historico.iterator();
		
		removerTodos(table);
		
		while(iterador.hasNext()){
			String[] valor = iterador.next();
			adicionarItem(table,valor);
		}
	}
	
	private void adicionarItem(JTable tabela, String[] valor){
		DefaultTableModel dtm = (DefaultTableModel)tabela.getModel();
		String repouso = "";
		
		if(valor[4] == null){
			return;
		}
		if(valor[4].equals("true")){
			repouso = "Sim";
		}else{
			repouso = "Não";
		}
		
		dtm.insertRow(0,new Object[]{valor[0],valor[1],valor[2],valor[3],repouso});
		
	}
	
	private boolean verificaExistencia(JTable tabela, String id, int index){
		DefaultTableModel dtm = (DefaultTableModel)tabela.getModel();
		int linhas = dtm.getRowCount();
		
		for(int i=0;i<linhas;i++){
			if(dtm.getValueAt(i, index).equals(id)){
				return true;
			}
		}
		
		return false;
	}
	
	private void removerTodos(JTable tabela){
		DefaultTableModel dtm = (DefaultTableModel)tabela.getModel();
		for(int index=0;index < tabela.getModel().getRowCount();index++){
			dtm.removeRow(0);
		}
	}
}
