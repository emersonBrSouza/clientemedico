package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import controller.MedicoController;
import model.Paciente;
import model.Sensor;

public class MedicoView extends JFrame implements Observer{
	private String nomeMedico;
	private String crm;
	private MedicoController controller;
	private JTextField textField;
	private JTable tabelaPacientes;
	private JTable tabelaPacientesPropensos;
	private JTable tabelaPacientesSelecionados;
	private ListarPacientesPropensos listarPacientesPropensos;
	private ListarTodosPacientes listarTodosPacientes;
	private ListarPacientesSelecionados listarPacientesSelecionados;
	private ListarTodosPacientes verificarConexao;

	/**
	 * Create the frame.
	 * @param crm 
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public MedicoView(String nome,final String crm, final MedicoController controller) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		getContentPane().setBackground(Color.WHITE);
		this.nomeMedico = nome;
		this.crm = crm;
		this.controller = controller;
		
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 600);

		setVisible(true);
		setResizable(false);
		getContentPane().setLayout(null);
		
		new PacientesTotais().inicializar();
		new PacientesSelecionados().inicializar();
		new PacientesPropensos().inicializar();
		
		controller.addObserver(this);
		controller.listarTodosPacientes();
		controller.listarPacientesPropensos();
		controller.verificarConexao();
		
		listarTodosPacientes = new ListarTodosPacientes();
		Timer timerTodosPacientes = new Timer();
		timerTodosPacientes.schedule(listarTodosPacientes, 0,2000);
		
		listarPacientesPropensos = new ListarPacientesPropensos();
		Timer timerPacientesPropensos = new Timer();
		timerPacientesPropensos.schedule(listarPacientesPropensos, 0, 2000);
		

		listarPacientesSelecionados = new ListarPacientesSelecionados();
		Timer timerPacientesSelecionados = new Timer();
		timerPacientesSelecionados.schedule(listarPacientesSelecionados, 0,2000);
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				if(controller.logout(crm)){
					System.exit(0);
				}
			}
		});
	}

	
	private class verificarConexao extends TimerTask{
		public void run(){
			controller.verificarConexao();
		}
	}
	
	//Povoamento das tabelas
	private class ListarTodosPacientes extends TimerTask{
		public void run(){
			controller.listarTodosPacientes();
		}
	}
	
	private class ListarPacientesPropensos extends TimerTask{
		public void run(){
			controller.listarPacientesPropensos();
		}
	}
	
	private class ListarPacientesSelecionados extends TimerTask{
		public void run(){
			controller.listarPacientesSelecionados();
		}
	}
	
	//Atualizar Listas
	private void atualizarTodosPacientes(){
		ArrayList<Paciente> pacientes = controller.getTodosPacientes();
		Iterator<Paciente> iterador = pacientes.iterator();
		
		while(iterador.hasNext()){
			Paciente p = (Paciente) iterador.next();
			adicionarItem(tabelaPacientes,p);
		}
	}
	
	private void atualizarPacientesSelecionados(){
		ArrayList<Paciente> pacientes = controller.getPacientesSelecionados();
		Iterator<Paciente> iterador = pacientes.iterator();
		
		while(iterador.hasNext()){
			Paciente p = (Paciente) iterador.next();
			adicionarItem(tabelaPacientesSelecionados,p);
		}
	}
	
	private void atualizarPacientesPropensos(){
		ArrayList<Paciente> pacientes = controller.getPacientesPropensos();
		Iterator<Paciente> iterador = pacientes.iterator();
		
		while(iterador.hasNext()){
			Paciente p = (Paciente) iterador.next();
			adicionarItem(tabelaPacientesPropensos,p);
		}
	}
	
	
	private void monitorarEspecifico(JTable tabela) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{
		int index = tabela.getSelectedRow();
		String idPaciente = (String) tabela.getModel().getValueAt(index, 0);	
		new MonitoramentoView(this, idPaciente);
	}
	
	//Manipulação das tabelas
	private void selecionarPacientes(){
		
		int index = tabelaPacientes.getSelectedRow();
		String idPaciente = (String) tabelaPacientes.getModel().getValueAt(index, 0);
		removerItem(tabelaPacientes,index);
		controller.selecionarPaciente(idPaciente);
	}
	

	
	private void adicionarItem(JTable tabela, Paciente item){
		DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
		if(item == null){
			return;
		}
		
		if(tabela.equals(tabelaPacientes)){
			//Verifica se a pessoa já foi selecionada, em caso de positivo, não é adicionada à lista com todos
			if(!jaSelecionado( item) && buscarID(tabela,item) == -1){ 
				dtm.addRow(new Object[]{item.getSensor().getId(),item.getNome()});
			}
		}else {
			Sensor s = item.getSensor();
			String repouso = s.estaEmRepouso()?"Sim":"Não";
			
			int index = buscarID(tabela, item);
			if(index == -1){ //Se não existir, adiciona
				dtm.addRow(new Object[]{s.getId(),item.getNome(),s.getBatimentos(),s.getPressaoSistolica(),s.getPressaoDiastolica(),repouso});
			}else { //Se existir atualiza
				dtm.setValueAt(item.getNome(), index, 1);
				dtm.setValueAt(s.getBatimentos(), index, 2);
				dtm.setValueAt(s.getPressaoSistolica(), index, 3);
				dtm.setValueAt(s.getPressaoDiastolica(), index, 4);
				dtm.setValueAt(repouso, index, 5);	
			}
			
		}
		
	}
	private void removerItem(JTable tabela, int indexLinha){
		DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
		dtm.removeRow(indexLinha);
	}
	
	
	private boolean jaSelecionado(Paciente item){
		DefaultTableModel dtm = (DefaultTableModel) tabelaPacientesSelecionados.getModel();
		int n = dtm.getRowCount();
		
		for(int i=0;i<n;i++){
			if(dtm.getValueAt(i, 0).equals(item.getSensor().getId())){
				return true;
			}
		}
		
		return false;
	}
	
	private int buscarID(JTable tabela, Paciente item){
		DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
		int n = dtm.getRowCount();
		
		for(int i=0;i<n;i++){
			if(dtm.getValueAt(i, 0).equals(item.getSensor().getId())){
				return i;
			}
		}
		
		return -1;
	}
	
	private void removerTodos(JTable tabela){
		DefaultTableModel dtm = (DefaultTableModel)tabela.getModel();
		for(int index=0;index < tabela.getModel().getRowCount();index++){
			dtm.removeRow(0);
		}
	}
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Boolean){
			if(!MedicoController.getInstance().isConectadoAoServidor()){
				JOptionPane.showMessageDialog(null,"Você foi desconectado do servidor. O programa está tentando reconectar!");
				MedicoController.getInstance().verificarConexao();
				return;
			}
		}
		
		if(o instanceof MedicoController && !(arg instanceof HashMap)){
			
			if(((MedicoController) o).getPacientesPropensos() != null){
				if(((MedicoController) o).getPacientesPropensos().size() == 0){
					removerTodos(tabelaPacientesPropensos);
				}else {
					atualizarPacientesPropensos();
				}
				
			}
			
			if(((MedicoController) o).getPacientesSelecionados() !=null){
				
				if(((MedicoController) o).getPacientesSelecionados().size() == 0){
					removerTodos(tabelaPacientesSelecionados);
				}else {
					atualizarPacientesSelecionados();
				}
			}
	
			if(((MedicoController) o).getTodosPacientes() != null){
				if(((MedicoController) o).getTodosPacientes().size() == 0){
					removerTodos(tabelaPacientesSelecionados);
				}else {
					atualizarTodosPacientes();
				}
			}
			
			
		}
		
	}
	
	public MedicoController getController() {
		return this.controller;
	}

	// Classes auxiliares
	private class PacientesSelecionados{
		public JPanel inicializar(){
			JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setBounds(10, 309, 595, 251);
			panel.setBackground(getContentPane().getBackground());
			getContentPane().add(panel);
			
			JTextPane txtpnPacientesSelecionados = new JTextPane();
			txtpnPacientesSelecionados.setFont(new Font("Roboto Light", Font.PLAIN, 14));
			txtpnPacientesSelecionados.setBounds(0, 0, 360, 20);
			panel.add(txtpnPacientesSelecionados);
			txtpnPacientesSelecionados.setEditable(false);
			txtpnPacientesSelecionados.setBackground(panel.getBackground());
			txtpnPacientesSelecionados.setText("Pacientes selecionados");

			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(0, 26, 595, 225);
			panel.add(scrollPane);
			panel.add(txtpnPacientesSelecionados);
			
			tabelaPacientesSelecionados = new JTable();
			tabelaPacientesSelecionados.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID do Dispositivo", "Nome do paciente", "Batimentos", "Pressão Sistólica", "Pressão Diastólica", "Em repouso?"
				}
			){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				boolean[] columnEditables = new boolean[] {
					false, false,false,false,false,false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
			
			tabelaPacientesSelecionados.addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e){
					if (e.getClickCount() == 2 && !e.isConsumed()) {
					     e.consume();
					     try {
							monitorarEspecifico(tabelaPacientesSelecionados);
						} catch (ClassNotFoundException | InstantiationException | IllegalAccessException| UnsupportedLookAndFeelException e1) {
							
						}
					}
				}
			});
			
			tabelaPacientesSelecionados.getColumnModel().getColumn(0).setPreferredWidth(100);
			tabelaPacientesSelecionados.getColumnModel().getColumn(1).setPreferredWidth(100);
			tabelaPacientesSelecionados.getColumnModel().getColumn(2).setPreferredWidth(60);
			tabelaPacientesSelecionados.getColumnModel().getColumn(3).setPreferredWidth(85);
			tabelaPacientesSelecionados.getColumnModel().getColumn(4).setPreferredWidth(85);
			tabelaPacientesSelecionados.getColumnModel().getColumn(5).setPreferredWidth(60);
			
			scrollPane.setViewportView(tabelaPacientesSelecionados);
						
			return panel;
		}
	}
	
	private class PacientesTotais{
		
		public JPanel inicializar(){
			JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
			panel.setBackground(getContentPane().getBackground());
			panel.setBounds(615, 11, 269, 549);
			getContentPane().add(panel);
			
			JTextPane txtpnPacientesConectados = new JTextPane();
			txtpnPacientesConectados.setText("Pacientes Conectados");
			txtpnPacientesConectados.setFont(new Font("Roboto Light", Font.PLAIN, 14));
			txtpnPacientesConectados.setBackground(panel.getBackground());
			txtpnPacientesConectados.setEditable(false);
			txtpnPacientesConectados.setBounds(10, 5, 200, 20);
			panel.add(txtpnPacientesConectados);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setBounds(10, 61, 249, 477);
			panel.add(scrollPane);
			
			tabelaPacientes = new JTable();
			tabelaPacientes.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID do Dispositivo", "Nome do Paciente"
				}
			) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				boolean[] columnEditables = new boolean[] {
					false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
			
			tabelaPacientes.addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e){
					if (e.getClickCount() == 2 && !e.isConsumed()) {
					     e.consume();
					     selecionarPacientes();
					}
				}
			});
			scrollPane.setViewportView(tabelaPacientes);
			
			
			return panel;
		}
	}

	private class PacientesPropensos{
		
		public JPanel inicializar(){
			
			JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setBounds(10, 11, 595, 287);
			panel.setBackground(getContentPane().getBackground());
			getContentPane().add(panel);
			
			JTextPane txtpnPacientesPropensos = new JTextPane();
			txtpnPacientesPropensos.setFont(new Font("Roboto Light", Font.PLAIN, 14));
			txtpnPacientesPropensos.setBounds(0, 0, 360, 20);
			txtpnPacientesPropensos.setEditable(false);
			txtpnPacientesPropensos.setBackground(panel.getBackground());
			txtpnPacientesPropensos.setText("Pacientes propensos \u00E0 ataque card\u00EDaco");
			
			JScrollPane scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(0, 27, 595, 260);
			panel.add(scrollPane_1);
			panel.add(txtpnPacientesPropensos);
			
			tabelaPacientesPropensos = new JTable();
			tabelaPacientesPropensos.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID do Dispositivo", "Nome do paciente", "Batimentos", "Pressão Sistólica", "Pressão Diastólica", "Em repouso?"
				}
			){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				boolean[] columnEditables = new boolean[] {
					false, false,false,false,false,false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
			
			tabelaPacientesPropensos.addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e){
					if (e.getClickCount() == 2 && !e.isConsumed()) {
					     e.consume();
					     try {
							monitorarEspecifico(tabelaPacientesPropensos);
						} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {

						}
					}
				}
			});
			tabelaPacientesPropensos.getColumnModel().getColumn(0).setPreferredWidth(100);
			tabelaPacientesPropensos.getColumnModel().getColumn(1).setPreferredWidth(100);
			tabelaPacientesPropensos.getColumnModel().getColumn(2).setPreferredWidth(60);
			tabelaPacientesPropensos.getColumnModel().getColumn(3).setPreferredWidth(85);
			tabelaPacientesPropensos.getColumnModel().getColumn(4).setPreferredWidth(85);
			tabelaPacientesPropensos.getColumnModel().getColumn(5).setPreferredWidth(60);
			
			scrollPane_1.setViewportView(tabelaPacientesPropensos);
			
			return panel;
		}
	}

	
}
