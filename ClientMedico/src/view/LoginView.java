package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.MaskFormatter;

import controller.MedicoController;

public class LoginView {

	private JFrame frmLoginIot;
	private JTextField usuario;
	private JPasswordField password;
	private MedicoController controller = MedicoController.getInstance() ;
	private JTextPane txtpnNome;
	private JTextPane txtpnSenha;
	private JTextField enderecoIP;
	private JTextPane txtpnEndereo;
	private JTextField porta;
	private JTextPane txtpnPorta;
	private JTextPane txtpnUsurioEouSenha;
	private JFormattedTextField crm;
	private JTextPane txtpnCrm;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginView window = new LoginView();
					window.frmLoginIot.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public LoginView() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	private void initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		frmLoginIot = new JFrame();
		frmLoginIot.getContentPane().setBackground(new Color(102, 204, 255));
		frmLoginIot.setTitle("Login - IOT");
		frmLoginIot.setBounds(100, 100, 305, 473);
		frmLoginIot.getContentPane().setLayout(null);
		
		usuario = new JTextField();
		usuario.setBounds(21, 198, 255, 20);
		frmLoginIot.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		password = new JPasswordField();
		password.setBounds(21, 260, 255, 20);
		frmLoginIot.getContentPane().add(password);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setFont(new Font("Roboto Medium", Font.PLAIN, 14));
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					login();
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					System.err.println("Erro Look and Feel");
				}
			}
		});
		btnEntrar.setBounds(71, 389, 156, 33);
		frmLoginIot.getContentPane().add(btnEntrar);
		
		txtpnUsurioEouSenha = new JTextPane();
		txtpnUsurioEouSenha.setToolTipText("Erro ao fazer login. Tente Novamente");
		txtpnUsurioEouSenha.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnUsurioEouSenha.setVisible(false);
		txtpnUsurioEouSenha.setForeground(Color.RED);
		txtpnUsurioEouSenha.setFont(new Font("Roboto Light", Font.PLAIN, 13));
		txtpnUsurioEouSenha.setEditable(false);
		txtpnUsurioEouSenha.setBounds(31, 354, 245, 20);
		frmLoginIot.getContentPane().add(txtpnUsurioEouSenha);
		
		txtpnNome = new JTextPane();
		txtpnNome.setForeground(new Color(0, 0, 0));
		txtpnNome.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnNome.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnNome.setEditable(false);
		txtpnNome.setText("Nome");
		txtpnNome.setBounds(21, 167, 51, 20);
		frmLoginIot.getContentPane().add(txtpnNome);
		
		txtpnSenha = new JTextPane();
		txtpnSenha.setForeground(new Color(0, 0, 0));
		txtpnSenha.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnSenha.setEditable(false);
		txtpnSenha.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnSenha.setText("Senha");
		txtpnSenha.setBounds(21, 229, 51, 20);
		frmLoginIot.getContentPane().add(txtpnSenha);
		
		enderecoIP = new JTextField();
		enderecoIP.setBounds(21, 323, 156, 20);
		frmLoginIot.getContentPane().add(enderecoIP);
		enderecoIP.setColumns(10);
		
		txtpnEndereo = new JTextPane();
		txtpnEndereo.setForeground(new Color(0, 0, 0));
		txtpnEndereo.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnEndereo.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnEndereo.setEditable(false);
		txtpnEndereo.setText("Endere\u00E7o IP");
		txtpnEndereo.setBounds(21, 291, 89, 20);
		frmLoginIot.getContentPane().add(txtpnEndereo);
		
		porta = new JTextField();
		porta.setBounds(187, 323, 89, 20);
		frmLoginIot.getContentPane().add(porta);
		porta.setColumns(10);
		
		txtpnPorta = new JTextPane();
		txtpnPorta.setForeground(new Color(0, 0, 0));
		txtpnPorta.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnPorta.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnPorta.setEditable(false);
		txtpnPorta.setText("Porta");
		txtpnPorta.setBounds(187, 291, 51, 20);
		frmLoginIot.getContentPane().add(txtpnPorta);
		
		MaskFormatter crmFormatter = null;
		try {
			crmFormatter = new MaskFormatter("########-#");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		crm = new JFormattedTextField(crmFormatter);
		crm.setBounds(21, 136, 255, 20);
		frmLoginIot.getContentPane().add(crm);
		crm.setColumns(10);
		
		txtpnCrm = new JTextPane();
		txtpnCrm.setForeground(new Color(0, 0, 0));
		txtpnCrm.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnCrm.setEditable(false);
		txtpnCrm.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnCrm.setText("CRM");
		txtpnCrm.setBounds(21, 105, 86, 20);
		frmLoginIot.getContentPane().add(txtpnCrm);
		
		String fs = System.getProperty("file.separator");
		
		ImageIcon iconeAparelho = new ImageIcon(getClass().getResource("/heart.png"));
		JLabel lblNewLabel = new JLabel(iconeAparelho);
		lblNewLabel.setBounds(113, 24, 64, 64);
		frmLoginIot.getContentPane().add(lblNewLabel);
		
		JTextPane txtpnI = new JTextPane();
		txtpnI.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnI.setFont(new Font("Source Code Pro", Font.BOLD, 64));
		txtpnI.setEditable(false);
		txtpnI.setText("I");
		txtpnI.setBounds(65, 11, 48, 70);
		frmLoginIot.getContentPane().add(txtpnI);
		
		JTextPane txtpnT = new JTextPane();
		txtpnT.setFont(new Font("Source Code Pro", Font.BOLD, 64));
		txtpnT.setBackground(frmLoginIot.getContentPane().getBackground());
		txtpnT.setText("T");
		txtpnT.setBounds(187, 11, 48, 70);
		frmLoginIot.getContentPane().add(txtpnT);
		frmLoginIot.setResizable(false);
		frmLoginIot.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@SuppressWarnings("deprecation")
	private void login() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{
		
		if(!campoVazio(crm.getText()) && !campoVazio(usuario.getText()) && !campoVazio(password.getText()) && !campoVazio(enderecoIP.getText()) && !campoVazio(porta.getText())){
			controller.setCRM(crm.getText());
			if(controller.login(usuario.getText(), password.getText(),enderecoIP.getText(),Integer.parseInt(porta.getText()))){
				frmLoginIot.dispose();
				new MedicoView(usuario.getText(),crm.getText(),controller);
			}else{
				txtpnUsurioEouSenha.setVisible(true);
				txtpnUsurioEouSenha.setText("Erro ao fazer login. Tente Novamente");
			}
		}else{
			JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
		}
		
	}
	
	private boolean campoVazio(String texto){
		if(texto.isEmpty() || texto.trim().isEmpty() || texto.trim().length() == 1){
			return true;
		}else{
			return false;
		}	
	}
}
