package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.MedicoController;
import model.Paciente;


public class MonitoramentoView extends JFrame implements Observer{

	private JPanel contentPane;
	private JTable table;
	private MedicoView viewPai;
	private JTextPane valorUltimaAtualizacao;
	private JTextPane valorRepouso;
	private JTextPane valorDiastolica;
	private JTextPane valorSistolica;
	private JTextPane valorBatimentos;
	private Paciente paciente;
	private String idPaciente;
	private MedicoController controller;
	private JTextPane campoNome;
	private TimerTask atualizador;
	private JTextPane campoID;
	private JLabel labelRepouso;
	private JButton btnVisualizarHistricoDe;

	
	/**
	 * Create the frame.
	 */
	public MonitoramentoView(MedicoView viewPai,final String idPaciente) {
		setResizable(false);
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 720, 530);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		this.viewPai = viewPai;
		
		this.idPaciente = idPaciente;
		this.controller = viewPai.getController();
		controller.addObserver(this);
		
		setVisible(true);
		
		JTextPane txtpnNome = new JTextPane();
		txtpnNome.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnNome.setBackground(contentPane.getBackground());
		txtpnNome.setEditable(false);
		txtpnNome.setText("Nome:");
		txtpnNome.setBounds(20, 50, 52, 20);
		contentPane.add(txtpnNome);
		
		campoNome = new JTextPane();
		campoNome.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		campoNome.setBackground(contentPane.getBackground());
		campoNome.setEditable(false);
		campoNome.setBounds(77, 50, 151, 20);
		contentPane.add(campoNome);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 265, 684, 215);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 11));
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Hora","Batimentos","Pressão Sistólica","Pressão Diastólica","Em repouso"
			}){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				boolean[] columnEditables = new boolean[] {
					false, false,false,false,false,false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
		});
		scrollPane.setViewportView(table);
		
		JTextPane txtpnId = new JTextPane();
		txtpnId.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnId.setBackground(contentPane.getBackground());
		txtpnId.setEditable(false);
		txtpnId.setText("ID:");
		txtpnId.setBounds(20, 19, 21, 20);
		contentPane.add(txtpnId);
		
		campoID = new JTextPane();
		campoID.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		campoID.setBackground(contentPane.getBackground());
		campoID.setEditable(false);
		campoID.setBounds(51, 19, 207, 20);
		contentPane.add(campoID);
		
		JTextPane txtpnHistrico = new JTextPane();
		txtpnHistrico.setFont(new Font("Roboto Light", Font.PLAIN, 16));
		txtpnHistrico.setBackground(contentPane.getBackground());
		txtpnHistrico.setEditable(false);
		txtpnHistrico.setText("Hist\u00F3rico");
		txtpnHistrico.setBounds(10, 234, 74, 20);
		contentPane.add(txtpnHistrico);
		
		JTextPane txtpnInformaesAtuais = new JTextPane();
		txtpnInformaesAtuais.setFont(new Font("Roboto Light", Font.PLAIN, 18));
		txtpnInformaesAtuais.setBackground(contentPane.getBackground());
		txtpnInformaesAtuais.setEditable(false);
		txtpnInformaesAtuais.setText("Informa\u00E7\u00F5es Atuais");
		txtpnInformaesAtuais.setBounds(20, 81, 165, 28);
		contentPane.add(txtpnInformaesAtuais);
		
		valorBatimentos = new JTextPane();
		valorBatimentos.setFont(new Font("Roboto Thin", Font.PLAIN, 55));
		valorBatimentos.setBackground(contentPane.getBackground());
		valorBatimentos.setEditable(false);
		valorBatimentos.setBounds(94, 129, 106, 64);
		contentPane.add(valorBatimentos);
		
		valorSistolica = new JTextPane();
		valorSistolica.setFont(new Font("Roboto Thin", Font.PLAIN, 55));
		valorSistolica.setBackground(contentPane.getBackground());
		valorSistolica.setEditable(false);
		valorSistolica.setBounds(304, 129, 64, 64);
		contentPane.add(valorSistolica);
		
		valorDiastolica = new JTextPane();
		valorDiastolica.setFont(new Font("Roboto Thin", Font.PLAIN, 55));
		valorDiastolica.setBackground(contentPane.getBackground());
		valorDiastolica.setEditable(false);
		valorDiastolica.setBounds(393, 129, 64, 64);
		contentPane.add(valorDiastolica);
		
		valorRepouso = new JTextPane();
		valorRepouso.setEnabled(false);
		valorRepouso.setBackground(contentPane.getBackground());
		valorRepouso.setVisible(false);
		valorRepouso.setBounds(521, 211, 79, 20);
		contentPane.add(valorRepouso);
		
		JTextPane txtpnltimaAtualizao = new JTextPane();
		txtpnltimaAtualizao.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		txtpnltimaAtualizao.setBackground(contentPane.getBackground());
		txtpnltimaAtualizao.setEditable(false);
		txtpnltimaAtualizao.setText("\u00DAltima Atualiza\u00E7\u00E3o:");
		txtpnltimaAtualizao.setBounds(380, 55, 140, 20);
		contentPane.add(txtpnltimaAtualizao);
		
		valorUltimaAtualizacao = new JTextPane();
		valorUltimaAtualizacao.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		valorUltimaAtualizacao.setBackground(contentPane.getBackground());
		valorUltimaAtualizacao.setEditable(false);
		valorUltimaAtualizacao.setBounds(511, 55, 183, 20);
		contentPane.add(valorUltimaAtualizacao);
		
		
		
		String fs = System.getProperty("file.separator");
		ImageIcon iconeCoracao = new ImageIcon(getClass().getResource("/heart-01.png"));
		
		JLabel labelCoracao = new JLabel(iconeCoracao);
		labelCoracao.setBounds(20, 129, 64, 64);
		contentPane.add(labelCoracao);
		
		ImageIcon iconeAparelho = new ImageIcon(getClass().getResource("/aparelho-01.png"));
		JLabel labelAparelho = new JLabel(iconeAparelho);
		labelAparelho.setBounds(240, 129, 64, 64);
		contentPane.add(labelAparelho);
		
		JTextPane separador = new JTextPane();
		separador.setBackground(contentPane.getBackground());
		separador.setFont(new Font("Roboto Thin", Font.PLAIN, 55));
		separador.setText("/");
		separador.setEditable(false);
		separador.setBounds(380, 129, 27, 64);
		contentPane.add(separador);
		
		ImageIcon iconeRepouso1 = new ImageIcon(getClass().getResource("/repouso.png"));
		labelRepouso = new JLabel(iconeRepouso1);
		labelRepouso.setBounds(521, 129, 64, 64);
		contentPane.add(labelRepouso);
		
		btnVisualizarHistricoDe = new JButton("Visualizar Hist\u00F3rico de Risco");
		btnVisualizarHistricoDe.setEnabled(false);
		btnVisualizarHistricoDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new HistoricoView(idPaciente);
			}
		});
		btnVisualizarHistricoDe.setForeground(new Color(0, 0, 0));
		btnVisualizarHistricoDe.setBackground(new Color(51, 204, 255));
		btnVisualizarHistricoDe.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		btnVisualizarHistricoDe.setBounds(375, 16, 225, 23);
		contentPane.add(btnVisualizarHistricoDe);
		
		atualizador = new Atualizador();
		final Timer timer = new Timer();
		timer.schedule(atualizador, 1000, 1000);
		
		this.addWindowListener(new WindowAdapter(){
			
			@Override
			public void windowClosing(WindowEvent e){
				timer.cancel();
				atualizador.cancel();
			}
		});
		
	}
	
	private void atualizarTabela(){
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		Vector<String> v = new Vector<String>();
		
		if(!valorUltimaAtualizacao.getText().isEmpty()){
			v.add(valorUltimaAtualizacao.getText());
			v.add(valorBatimentos.getText());
			v.add(valorSistolica.getText());
			v.add(valorDiastolica.getText());
			v.add(valorRepouso.getText());
			dtm.insertRow(0, v);
		}
		
	}

	private void atualizarValores(){
		SimpleDateFormat dtf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		
		if(this.getTitle().isEmpty() && campoNome.getText().isEmpty() && campoID.getText().isEmpty()){
			this.setTitle("Monitoramento: "+paciente.getNome());
			campoNome.setText(paciente.getNome());
			campoID.setText(idPaciente);
		}
		btnVisualizarHistricoDe.setEnabled(true);
		valorUltimaAtualizacao.setText(dtf.format(date));
		valorBatimentos.setText(Integer.toString(paciente.getSensor().getBatimentos()));
		valorSistolica.setText(Integer.toString(paciente.getSensor().getPressaoSistolica()));
		valorDiastolica.setText(Integer.toString(paciente.getSensor().getPressaoDiastolica()));
		valorRepouso.setText(paciente.getSensor().estaEmRepouso()?"Sim":"Não");
		
		String fs = System.getProperty("file.separator");
		ImageIcon iconeRepouso1 = new ImageIcon(getClass().getResource("/repouso.png"));
		ImageIcon iconeRepouso2 = new ImageIcon(getClass().getResource("/movimento.png"));
		labelRepouso.setIcon(paciente.getSensor().estaEmRepouso()?iconeRepouso1:iconeRepouso2);
	}
	@Override
	public void update(Observable o, Object arg) {
		
		if(o instanceof MedicoController && arg instanceof Map){
			this.paciente = controller.getPacientesEspecificos().get(this.idPaciente);
			atualizarTabela();
			atualizarValores();
		}
		
	}
	
	private class Atualizador extends TimerTask{
		@Override
		public void run(){
				controller.monitorarEspecifico(idPaciente);
		}
	}
}
