package controller;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Scanner;

import model.Paciente;
import util.Acao;

public class MedicoController extends Observable{

	private static MedicoController controller;
	private static String id;
	private String enderecoIP;
	private int porta;
	private ArrayList<Paciente> todosPacientes;
	private ArrayList<Paciente> pacientesSelecionados;
	private ArrayList<Paciente> pacientesPropensos;
	private ArrayList<String[]> historico;
	private Map<String,Paciente> pacientesEspecificos = new HashMap<String,Paciente>();
	private DatagramSocket socketUDPMedico;
	private boolean conectadoAoServidor = false;
	
        
        //Singleton
        private MedicoController(){}
	public static MedicoController getInstance(){
		if(controller == null){
			controller = new MedicoController(); 
		}
		return controller;
	}
	
	
	/**
	 * Realiza o login do médico
	 * 
	 * @param username - O nome do usu�rio
	 * @param password - A senha do usu�rio
	 * @param enderecoIP - O endere�o IP do servidor
	 * @param porta - A porta do servidor
	 * @return true - Se o login for bem-sucedido
	 * @return false - Se o login for mal-sucedido*/
	public boolean login(String username,String password,String enderecoIP, int porta){
		this.enderecoIP = enderecoIP;
		this.porta = porta;
		
		Acao acao = new Acao("--login",username+":"+password+":"+id);
		boolean resposta = false;
		
		try {
			Socket clienteSocket = new Socket(enderecoIP,porta);
			ObjectOutputStream saida = new ObjectOutputStream(clienteSocket.getOutputStream());
			BufferedReader entrada = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));
			
			saida.writeObject(acao);
			saida.flush();
			
			String mensagem = entrada.readLine();
			
			if(mensagem.contains("--login-accepted")){
				resposta = true;

			} else if(mensagem.contains("--login-refused")){
				resposta = false;
			}
			
			clienteSocket.close();
			
		} catch (UnknownHostException e) {
			System.err.println("Servidor n�o encontrado");
			return false;
		} catch (SocketException e) {
			System.err.println("Erro no canal de comunica��o");
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Erro no envio dos dados");
			return false;
		} catch (Exception e){
			System.err.println("Erro desconhecido");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Realiza o login do médico
	 * 
	 * @param idMedico - O id do usu�rio
	 * @return true - Se o login for bem-sucedido
	 * @return false - Se o login for mal-sucedido*/
	public boolean logout(String idMedico){
		
		Acao acao = new Acao("--logout","Médico"+":"+idMedico);
		boolean resposta = false;
		
		try {
			Socket clienteSocket = new Socket(enderecoIP,porta);
			ObjectOutputStream saida = new ObjectOutputStream(clienteSocket.getOutputStream()); //Obtém o canal de saída
			BufferedReader entrada = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));  //Obtém o canal de entrada
			
                        //Escreve a ação e envia
			saida.writeObject(acao);
			saida.flush();
			
			String mensagem = entrada.readLine();
			
			if(mensagem.contains("--logout-accepted")){
                            resposta = true;

			}
                        
			clienteSocket.close();
			
		} catch (UnknownHostException e) {
			System.err.println("Servidor não encontrado");
		} catch (SocketException e) {
			System.err.println("Erro no canal de comunicação");
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Erro no envio dos dados");
		}
		
		return true;
	}
	
        /**
         * Verifica periodicamente o estado da conexão
         */
	public void verificarConexao(){
            new Thread(){
                public void run(){
                    try {
                        while(true){
                            sleep(3000); //A cada três segundos um ping é enviado
                            Socket clienteSocket = new Socket(enderecoIP,porta);

                            OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
                            clienteSocket.setSoTimeout(8000); // Define o tempo limite para a resposta.
                            saida.write(serializarMensagens(new Acao("--ping",id))); // "Pinga" o servidor
                            saida.flush();

                            InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
                            Object resposta = ((ObjectInputStream) entrada).readObject(); // Lê o objeto da entrada

                            if(resposta instanceof String){ //Verifica a resposta do servidor 
                                if(resposta.equals("ok")){
                                    setConectadoAoServidor(true);
                                    setChanged();
                                    notifyObservers(isConectadoAoServidor()); //Notifica aos observadores
                                }
                            }
                            saida.close();
                            clienteSocket.close();

                        }	
                    } catch(SocketTimeoutException e){
                            setConectadoAoServidor(false);
                            setChanged();
                            notifyObservers(isConectadoAoServidor());
                    } catch(SocketException e){
                            setConectadoAoServidor(false);
                            setChanged();
                            notifyObservers(isConectadoAoServidor());
                    } catch (InterruptedException | IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                    }
                }
            }.start();
	}
	
        /**
         * Solicita ao servidor a seleção de um paciente
         * 
         * @param idPaciente - O ID do paciente selecionado
         */
	public void selecionarPaciente(String idPaciente){
		enviarUDP(new Acao("--select",id+":"+idPaciente)); // Cria a ação a ser executada e envia
	}
        
        /**
         * Solicita ao servidor a lista de todos os pacientes
         */
	public void listarTodosPacientes(){ 
		enviarUDP(new Acao("--list","-all")); // Cria a ação a ser executada e envia
	}
	
        /**
         * Solicita ao servidor a lista dos pacientes selecionados
         */
	public void listarPacientesSelecionados(){
		enviarUDP(new Acao("--list","-selected:"+id)); // Cria a ação a ser executada e envia
	}
	
        /**
         * Solicita ao servidor a lista dos pacientes propensos
         */
	public void listarPacientesPropensos(){
		enviarUDP(new Acao("--list","-danger")); // Cria a ação a ser executada e envia
	}

        /**
         * Solicita ao servidor os dados de pacientes específicos
         * 
         * @param idPaciente - O ID do paciente monitorado
         */
	public void monitorarEspecifico(String idPaciente){
		enviarUDP(new Acao("--watch",id+":"+idPaciente)); // Cria a ação a ser executada e envia
	}
	
        /**
         * Envia uma requisição ao servidor
         */
	public void enviarUDP(Object mensagem){
		byte[] dados = serializarMensagens(mensagem);
		
		try {
                    socketUDPMedico = new DatagramSocket();
                    DatagramPacket pacote = new DatagramPacket(dados,dados.length,InetAddress.getByName(enderecoIP),porta); //Configura o pacote
                    socketUDPMedico.send(pacote); //Envia a mensagem
                    ouvirUDP();
		} catch (UnknownHostException e) {
                    System.err.println("Servidor não encontrado");
		} catch (SocketException e) {
                    System.err.println("Erro no canal de comunicação");
		} catch (IOException e) {
                    System.err.println("Erro no envio dos dados");
		}
	}
	
        /**
         * Recebe as mensagens que chegam via UDP
         */
	private void ouvirUDP(){
            new Thread(){
                public void run(){
                    while(true){
                        try {
                            byte[] dadosRecebidos = new byte[1024];
                            DatagramPacket pacote = new DatagramPacket(dadosRecebidos,dadosRecebidos.length); //Configura o pacote
                            socketUDPMedico.receive(pacote); //Aguarda o recebimento do dado
                            new Thread(new Interpretador(pacote)).start(); // Inicia uma thread para o processamento do pacote recebido
                        } catch(EOFException e){
                            socketUDPMedico.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
	}
	
        /**
        * Transforma uma mensagem em um array de bytes.
        * 
        * @param mensagem - A mensagem a ser transformada.
        * @return byte[] - Um array de bytes convertidos
        */
	public byte[] serializarMensagens(Object mensagem){
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		try {
			ObjectOutput out = new ObjectOutputStream(b);
			out.writeObject(mensagem);
			out.flush();
			return b.toByteArray();
		} catch (IOException e) {
			System.err.println("Erro no envio/recebimento dos dados");
		}
		return null;
		
	}	
	
	public ArrayList<Paciente> getTodosPacientes() {
		return todosPacientes;
	}

        /**
         * Define a lista de todos os pacientes e notifica aos observadores
         * 
         * @param pacientes - A lista de pacientes
         */
	protected void setTodosPacientes(ArrayList<Paciente> pacientes){
		this.todosPacientes = pacientes;
		setChanged();
		notifyObservers();
	}
	
	public ArrayList<Paciente> getPacientesSelecionados() {
		return pacientesSelecionados;
	}

        /**
         * Define a lista de todos os pacientes selecionados e notifica aos observadores
         * 
         * @param pacientes - A lista de pacientes
         */
	protected void setPacientesSelecionados(ArrayList<Paciente> pacientes){
		this.pacientesSelecionados = pacientes;
		setChanged();
		notifyObservers();
	}
	public ArrayList<Paciente> getPacientesPropensos() {
		return pacientesPropensos;
	}
	
        /**
         * Define a lista de todos os pacientes propensos e notifica aos observadores
         * 
         * @param pacientes - A lista de pacientes
         */
	protected void setPacientesPropensos(ArrayList<Paciente> pacientes){
		this.pacientesPropensos = pacientes;
		setChanged();
		notifyObservers();
		
	}
	public Map<String,Paciente> getPacientesEspecificos() {
		return pacientesEspecificos;
	}
        
	protected void setPacientesEspecificos(Map<String,Paciente> pacientesEspecificos) {
		this.pacientesEspecificos = pacientesEspecificos;
	}

	public void setCRM(String crm) {
		MedicoController.id = crm;
	}
	
        /**
         * Notifica aos observadores sobre uma mudança nos dados de um paciente específico
         * 
         * @param id - O id do paciente
         * @param paciente - Os dados do paciente.
         */
	public void informaMudanca(String id,Paciente paciente){
		getPacientesEspecificos().put(id,paciente);
		setChanged();
		MedicoController.getInstance().notifyObservers(getPacientesEspecificos());
	}

        /**
         * Solicita ao servidor o histórico de um paciente específico
         * 
         * @param idPaciente - O id do paciente que terá o histórico recuperado
         */
	public void recuperarHistorico(String idPaciente) {
		
            Acao acao = new Acao("--history",idPaciente); //Instancia a ação a ser requisitada

            try {
                final Socket clienteSocket = new Socket(enderecoIP,porta);
                ObjectOutputStream saida = new ObjectOutputStream(clienteSocket.getOutputStream()); //Obtém o canal de saída

                //Escreve a mensagem e envia
                saida.writeObject(acao);
                saida.flush();

                new Thread(){

                    public void run(){
                        try {
                            InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); //Obtém o canal de entrada

                            Object mensagem = ((ObjectInput) entrada).readObject(); //Lê a entrada
                            ArrayList<String[]> historico = new ArrayList<String[]>();

                            if(mensagem instanceof ArrayList){ //Verifica a resposta
                                    historico = (ArrayList<String[]>) mensagem;
                                    setHistorico(historico); //Salva o histórico
                            }

                        }catch (IOException | ClassNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }.start();


            } catch (UnknownHostException e) {
                    System.err.println("Servidor não encontrado");
            } catch (SocketException e) {
                    System.err.println("Erro no canal de comunicação");
            } catch (IOException e) {
                    e.printStackTrace();
                    System.err.println("Erro no envio dos dados");
            }
				
	}
	
	public ArrayList<String[]> getHistorico(){
		return this.historico;
	}

        /**
         * Define o histórico e atualiza os observadores
         * @param historico - O histórico a ser armazenado
         */
	private void setHistorico(ArrayList<String[]> historico){
		this.historico = historico;
		setChanged();
		notifyObservers(this.historico);
	}
        
        /**
         * Retorna o estado da conexão com o servidor
         * 
         * @return true - Se estiver conectado ao servidor
         * @return false - Se não estiver conectado ao servidor
         */
	public Boolean isConectadoAoServidor() {
		return conectadoAoServidor;
	}

	public void setConectadoAoServidor(boolean conectadoAoServidor) {
		this.conectadoAoServidor = conectadoAoServidor;
	}
}
