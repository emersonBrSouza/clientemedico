package controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.util.ArrayList;

import model.Paciente;
import util.MensagemUDP;

public class Interpretador implements Runnable{

	private DatagramPacket pacote;
	private MedicoController controller;
        
        /**
         * Construtor da classe
         * 
         * @param pacote - O pacote recebido do servidor
         */
	public Interpretador(DatagramPacket pacote){
		this.pacote = pacote;
	}
	@Override
	public void run() {
                // Verifica a mensagem recebida e direciona o fluxo da aplicação
		Object o = desserializarMensagem(pacote.getData());
		if(o instanceof MensagemUDP){ 
			Object mensagem = ((MensagemUDP) o).getMensagem();
			String flag = ((MensagemUDP) o).getCabecalho();
			
			if(mensagem instanceof ArrayList && flag.equals("--all")){
				MedicoController.getInstance().setTodosPacientes((ArrayList<Paciente>)mensagem);
			} else if(mensagem instanceof ArrayList && flag.equals("--selected")){
				MedicoController.getInstance().setPacientesSelecionados((ArrayList<Paciente>)mensagem);
			} else if(mensagem instanceof ArrayList && flag.equals("--danger")){
				MedicoController.getInstance().setPacientesPropensos((ArrayList<Paciente>)mensagem);
			} else if(mensagem instanceof Paciente && flag.equals("--single")){
				
				String id = ((Paciente) mensagem).getSensor().getId();
				MedicoController.getInstance().informaMudanca(id,(Paciente)mensagem);
			}
			
		}
	}

        /**
        * Transforma um array de bytes em um Object.
        * 
        * @param data - O array de bytes recebido.
        * @return Object - O objeto desserializado.
        */
	private Object desserializarMensagem(byte[] data) {
		ByteArrayInputStream mensagem = new ByteArrayInputStream(data);
		
		try {
			ObjectInput leitor = new ObjectInputStream(mensagem);
			return (Object)leitor.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	    /**
        * Transforma uma mensagem em um array de bytes.
        * 
        * @param mensagem - A mensagem a ser transformada.
        * @return byte[] - Um array de bytes convertidos
        */
        
	public byte[] serializarMensagens(Object mensagem){
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		try {
                        //Transforma a mensagem em um array de bytes
			ObjectOutput out = new ObjectOutputStream(b);
			out.writeObject(mensagem);
			out.flush();
			return b.toByteArray();
		} catch (IOException e) {
			System.err.println("Erro no envio/recebimento dos dados");
		}
		return null;
		
	}
}
