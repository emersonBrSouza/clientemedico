package model;

import java.io.Serializable;

public class Sensor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private int batimentos;
	private int pressaoSistolica;
	private int pressaoDiastolica;
	private boolean emRepouso;
	
	/**
         * Construtor da classe
         * 
         * @param cpf - O CPF do cliente
         */
	public Sensor(String cpf){
		this.setId(cpf); 
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getBatimentos() {
		return batimentos;
	}
	public void setBatimentos(int batimentos) {
		this.batimentos = batimentos;
	}
	public int getPressaoSistolica() {
		return pressaoSistolica;
	}
	public void setPressaoSistolica(int pressaoSistolica) {
		this.pressaoSistolica = pressaoSistolica;
	}
	public int getPressaoDiastolica() {
		return pressaoDiastolica;
	}
	public void setPressaoDiastolica(int pressaoDiastolica) {
		this.pressaoDiastolica = pressaoDiastolica;
	}
	public boolean estaEmRepouso() {
		return emRepouso;
	}
	public void setEmRepouso(boolean emRepouso) {
		this.emRepouso = emRepouso;
	}
	
	
}
